<?php
// This script should receive a post from the contact form using ajax.
// It build an email message based on user submitted data e configuration parameters.

// the message is then sent using the mail server on localhost

$recaptchaSecret = 'your-secret-key';

$mailTo = 'yourmail@example.com'; // Contact form will send message to this address
$nameTo = 'WebSite staff';

$mailFrom = 'mailaddress@example.com'; // Must be a valid email address
$nameFrom = 'Coming Soon Page';

// Will be the subject of all messages that come from contact form
$mailSubject = 'Message from Coming Soon page';

// Email intro text. User message will be appended to it
$emailText = "You have a new message from your Coming Soon page \n=============================\n";

// Form field names and their translations. array ( variable name => Text to appear in the email  )
$fields = array('name' => 'Name', 'email' => 'Email', 'message' => 'Message');

//Text displayed on web page when message is sent
$okMessage = 'Message sent! Thanks for contacting us.';

// Generic error message displayed on web page when the mail couldn't be sent
$errorMessage = 'There was an error while submitting the form. Please try again later';

// When $debug is set to true the script returns a more detailed error message if the mail cannot be sent
$debug = false;

try {

    // If the user's email is missing stop here
    if (!array_key_exists('email', $_POST)) {
        throw new \Exception('Missing required email parameter');
    }

    require 'PHPMailer-master/PHPMailerAutoload.php';

    date_default_timezone_set('Etc/UTC');

    // Verify recaptcha using Google recaptcha PHP library
    require('recaptcha-master/src/autoload.php');

    if (!isset($_POST['g-recaptcha-response'])) {
        throw new \Exception('ReCaptcha is not set.');
    }
    $recaptcha = new \ReCaptcha\ReCaptcha($recaptchaSecret);

    // Validate the ReCaptcha field together with the user's IP address
    $recaptchaResponse = $recaptcha->verify($_POST['g-recaptcha-response'], $_SERVER['REMOTE_ADDR']);

    if (!$recaptchaResponse->isSuccess()) {
        throw new \Exception('ReCaptcha was not validated.');
    }

    // Create and config a new PHPMailer instance
    $mail = new PHPMailer;
    // Tell PHPMailer to use SMTP - requires a local mail server
    // Faster and safer than using mail()
    $mail->isSMTP();
    $mail->SMTPDebug  = 0;
    $mail->Host = 'localhost';
    $mail->Port = 25;

    // Use a fixed address in your own domain as the from address
    // **DO NOT** use the submitter's address here as it will be forgery
    // and will cause your messages to fail SPF checks
    $mail->setFrom($mailFrom, $nameFrom);

    // Send the message to yourself, or whoever should receive contact for submissions
    $mail->addAddress($mailTo, $nameTo);

    // Put the submitter's address in a reply-to header
    // This will fail if the address provided is invalid,
    // in which case we should ignore the whole request
    if (!$mail->addReplyTo($_POST['email'])) {
        throw new \Exception('Invalid email address, message ignored.');
    }
    $mail->Subject = $mailSubject;
    // Keep it simple - don't use HTML
    $mail->isHTML(false);

    // Build a simple message body
    $emailText = $emailText;

    foreach ($_POST as $key => $value) {
        // If the field exists in the $fields array, include it in the email
        if (isset($fields[$key])) {
            $emailText .= "$fields[$key]: $value\n";
        }
    }
    $mail->Body = $emailText;

    // Send the message, check for errors
    if (!$mail->send()) {
        throw new \Exception('I could not send the email.' . $mail->ErrorInfo);
    }
    // When message is successfully sent this will constitute the json response
    $response = 'success';
    $response_msg = $okMessage;
}
catch (\Exception $e)
{
    // We couldn't send the email, build a proper response for the browser
    $response = 'danger';

    // If debug option is enabled sent back a more specific error message
    if($debug) {
        $response_msg = $e->getMessage();
    }
    else {
        $response_msg = $errorMessage;
    }
}

// Return response in json
$responseArray = array('type' => $response, 'message' => $response_msg);
$encoded = json_encode($responseArray);

header('Content-Type: application/json');

echo $encoded;
?>
