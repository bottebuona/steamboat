var ajaxForm = (function () {

        var form = $('#contact-form');
        /* Display a bootstrap alert message in the form */

        var _alertDisplay = function (form, messageType, messageText) {
            /* Compose Bootstrap HTML alert box */
            var alertBox = '<div class="alert alert-' + messageType + ' alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' + messageText + '</div>';
            form.find('.messages').html(alertBox);
        };

        /* Bind form submit to ajax call */
        var init = function () {

            form.on('submit', function (e) {

                /* If the validator does not prevent form submit */
                if (!e.isDefaultPrevented()) {

                    var request = $.ajax({
                        type: 'POST',
                        url: 'php/contact.php',
                        data: form.serialize()
                    });

                    /* data = JSON object that contact.php returns */
                    request.done( function (data) {
                        /* Message type can be 'success' or 'danger' */
                        var messageType = data.type;
                        var messageText = data.message;

                        if (messageType && messageText) {
                            /* We got a valid response, display it on the page */
                            _alertDisplay(form, messageType, messageText);
                            /* Empty the form */
                            form[0].reset();

                            /* Reset the google recaptcha widget */
                            grecaptcha.reset();
                        }
                    });

                    request.fail( function(jqXHR, textStatus, errorThrown) {
                        if (debug === true) {
                            console.log(textStatus, errorThrown);
                        }
                        _alertDisplay(form, 'danger', ajaxErrorText);
                    });

                    return false;
                }
            });
        };

        /* return reference to init() function */
        return {
            init : init
        };
})();


var formValidator = (function () {
    /* This module use bootstrap-validator plugin ( http://1000hz.github.io/bootstrap-validator )
    support for recaptcha must be implementes through an hidden input field.
    As seen here: http://jsbin.com/kirevujexa/edit?html,js,output
    Solution suggested by the plugin's author here: https://github.com/1000hz/bootstrap-validator/issues/202 */

    var init = function() {
        var originalInputSelector = $.fn.validator.Constructor.INPUT_SELECTOR;
        $.fn.validator.Constructor.INPUT_SELECTOR = originalInputSelector + ', input[data-recaptcha]';

        /* update hidden field when recaptcha status change */
        window.verifyRecaptchaCallback = function (response) {
            $('input[data-recaptcha]').val(response).trigger('change')
        };
        window.expiredRecaptchaCallback = function () {
            $('input[data-recaptcha]').val("").trigger('change')
        };

        /* enable validator for the contact form */
        $('form#contact-form')
            .validator({
                custom: {
                    recaptcha: function ($el) {
                        if (!$el.val()) {
                            return "Please complete the captcha"
                        }
                    }
                }
            });
    }

    /* return reference to init() function */
    return {
        init : init
    };
})();


(function () {

    /* Boat Animation */
    var wheel = SVG.select('#wheel');
    wheel.animate(6000, '-').rotate(360).loop();

    /* Bind buttons outside forms */
    $('.notifyme-submit').click( function() { $('#notifyme-form').submit() } );
    $('.contact-submit').click( function() { $('#contact-form').submit() } );

    formValidator.init();
    ajaxForm.init();

    /* Use loadingoverlay plugin to display a spin icon
        while the page is waiting for an ajax response  */
    $(document).ajaxStart(function(){
        $.LoadingOverlay('show', {
            image       : '',
            fontawesome : 'fa fa-spinner fa-spin'
        });
    });

    $(document).ajaxStop(function(){
        $.LoadingOverlay('hide');
    });

}(window.jQuery, window, document));
